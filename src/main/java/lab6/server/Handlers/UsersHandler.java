package lab6.server.Handlers;//Created by Evgeny on 21.11.2017.

import com.sun.net.httpserver.HttpExchange;
import lab6.server.UserController;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;

public class UsersHandler extends BaseHandler {
    @Override
    public void myHandle(HttpExchange httpExchange) throws Throwable {

        printInfo(httpExchange);
        if (httpExchange.getRequestMethod().equalsIgnoreCase("GET")) {
            if ("/users".equals(httpExchange.getRequestURI().getPath())) {
                JSONObject responseBody = new JSONObject();
                for (UserController.User user : UserController.getInstance().getUsers()) {
                    responseBody.append("users", new JSONObject()
                            .put("id", user.getId())
                            .put("username", user.getUsername())
                            .put("online", (user.getOnlineStatus() != null) ? user.getOnlineStatus() : "null"));
                }
                byte[] responseBodyBytes = responseBody.toString().getBytes(StandardCharsets.UTF_8);
                httpExchange.getResponseHeaders().add("Content-Type", "application/json");
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, responseBodyBytes.length);
                httpExchange.getResponseBody().write(responseBodyBytes);
            } else {
                try {
                    int userId = Integer.valueOf(httpExchange.getRequestURI().getPath().substring(7));
                    UserController.User user = UserController.getInstance().getUser(userId);
                    if (user != null) {
                        JSONObject responseBody = new JSONObject();
                        responseBody.put("id", user.getId())
                                .put("username", user.getUsername())
                                .put("online", (user.getOnlineStatus() != null) ? user.getOnlineStatus() : "null");
                        byte[] responseBodyBytes = responseBody.toString().getBytes(StandardCharsets.UTF_8);
                        httpExchange.getResponseHeaders().add("Content-Type", "application/json");
                        httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, responseBodyBytes.length);
                        httpExchange.getResponseBody().write(responseBodyBytes);
                    } else {
                        httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_NOT_FOUND, -1);
                    }
                } catch (NumberFormatException | IndexOutOfBoundsException e) {
                    httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, -1);
                }
            }

        } else {
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_FORBIDDEN, -1);
        }
        httpExchange.close();
    }
}
