package lab6.server.Handlers;//Created by Evgeny on 21.11.2017.

import com.sun.net.httpserver.HttpExchange;
import lab6.server.UserController;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

public class LogoutHandler extends BaseHandler {

    @Override
    public void myHandle(HttpExchange httpExchange) throws Throwable {
        printInfo(httpExchange);
        if (httpExchange.getRequestMethod().equalsIgnoreCase("GET")) {
            UserController.getInstance().getUser(UUID.fromString(httpExchange.getPrincipal().getRealm())).logout();
            JSONObject responseBody = new JSONObject();
            responseBody.put("message", "bye!");

            byte[] responseBodyBytes = responseBody.toString().getBytes(StandardCharsets.UTF_8);
            httpExchange.getResponseHeaders().add("Content-Type", "application/json");
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, responseBodyBytes.length);
            httpExchange.getResponseBody().write(responseBodyBytes);
        } else {
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_FORBIDDEN, -1);
        }
        httpExchange.close();
    }
}
