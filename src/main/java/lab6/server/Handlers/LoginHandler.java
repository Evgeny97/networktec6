package lab6.server.Handlers;//Created by Evgeny on 21.11.2017.

import com.sun.net.httpserver.HttpExchange;
import lab6.server.UserController;
import org.json.HTTP;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

public class LoginHandler extends BaseHandler {

    @Override
    public void myHandle(HttpExchange httpExchange) throws Throwable {
        printInfo(httpExchange);
        if (httpExchange.getRequestMethod().equalsIgnoreCase("POST")) {
            JSONObject obj = new JSONObject(getRequestBodyAsString(httpExchange));
            String username = obj.getString("username");
            if (username == null || UserController.getInstance().isUserExistAndOnline(username)) {
                httpExchange.getResponseHeaders().add("WWW-Authenticate", "Token realm=’Username is already in use'");
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_UNAUTHORIZED, -1);
            } else {
                JSONObject responseBody = new JSONObject();
                UUID userUUID = UserController.getInstance().addUser(username);
                UserController.User user = UserController.getInstance().getUser(userUUID);
                responseBody.put("id", user.getId());
                responseBody.put("username", username);
                responseBody.put("online", true);
                responseBody.put("token", userUUID.toString());

                byte[] responseBodyBytes = responseBody.toString().getBytes(StandardCharsets.UTF_8);

                httpExchange.getResponseHeaders().add("Content-Type", "application/json");
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, responseBodyBytes.length);

                httpExchange.getResponseBody().write(responseBodyBytes);
            }
        }
        else{
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_FORBIDDEN, -1);
        }
        httpExchange.close();
    }
}
