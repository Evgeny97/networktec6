package lab6.server.Handlers;//Created by Evgeny on 21.11.2017.

import com.sun.net.httpserver.HttpExchange;
import lab6.server.MessagesController;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class MessagesHandler extends BaseHandler {

    private static Map<String, String> queryToMap(String query) {
        Map<String, String> result = new HashMap<>();
        if (query == null) {
            return result;
        }
        for (String param : query.split("&")) {
            String pair[] = param.split("=");
            if (pair.length > 1) {
                result.put(pair[0], pair[1]);
            } else {
                result.put(pair[0], "");
            }
        }
        return result;
    }

    @Override
    public void myHandle(HttpExchange httpExchange) throws Throwable {
        printInfo(httpExchange);
        if (httpExchange.getRequestMethod().equalsIgnoreCase("POST")) {
            JSONObject obj = new JSONObject(getRequestBodyAsString(httpExchange));
            String message = obj.getString("message");
            if (message == null) {
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_FORBIDDEN, -1);
            } else {
                JSONObject responseBody = new JSONObject();
                int id = MessagesController.getInstance().addMessage(message, httpExchange.getPrincipal().getUsername());
                responseBody.put("id", id);
                responseBody.put("message", message);
                byte[] responseBodyBytes = responseBody.toString().getBytes(StandardCharsets.UTF_8);
                httpExchange.getResponseHeaders().add("Content-Type", "application/json");
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, responseBodyBytes.length);
                httpExchange.getResponseBody().write(responseBodyBytes);
            }
        } else if (httpExchange.getRequestMethod().equalsIgnoreCase("GET")) {
            Map<String, String> params = queryToMap(httpExchange.getRequestURI().getQuery());
            int offset = 10;
            int count = 10;
            try {
                if (params != null) {
                    offset = (params.get("offset") == null || params.get("offset").isEmpty()) ? 10 : Integer.valueOf(params.get("offset"));
                    count = (params.get("count") == null || params.get("count").isEmpty()) ? 10 : Integer.valueOf(params.get("count"));
                    if (count > 100) {
                        count = 100;
                    }
                }
                MessagesController.Message[] messages = MessagesController.getInstance().getMessages(offset, count);
                JSONObject responseBody = new JSONObject();
                for (MessagesController.Message message : messages) {
                    responseBody.append("messages", new JSONObject()
                            .put("id", message.getId())
                            .put("message", message.getMessage())
                            .put("author", message.getAuthor()));
                }
                byte[] responseBodyBytes = responseBody.toString().getBytes(StandardCharsets.UTF_8);
                httpExchange.getResponseHeaders().add("Content-Type", "application/json");
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, responseBodyBytes.length);
                httpExchange.getResponseBody().write(responseBodyBytes);
            } catch (IndexOutOfBoundsException | IllegalArgumentException e) {
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, -1);
            }
        } else {
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_FORBIDDEN, -1);
        }
        httpExchange.close();
    }
}
